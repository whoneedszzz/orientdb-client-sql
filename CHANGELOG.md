## [202011271332]
### Changed
- Updated `whoneedszzz/orientdb-client` to [202011271258](https://gitlab.com/whoneedszzz/orientdb-client/-/blob/a1deeff72cf12e46872fe45c4faca81bb9eaa174/CHANGELOG.md)

## [202005102147]
### Changed
- Updated `whoneedszzz/orientdb-client` to [202005101819](https://gitlab.com/whoneedszzz/orientdb-client/-/blob/f97560181b054791b327d28f75ed501e15b2a243/CHANGELOG.md)

## [202005020039]
### Removed
- `keywords?` parameter from `query` and `command` as it was removed from `resultset->maps`
- Explicit `com.orientechnologies/orientdb-core` dependency in `deps.edn`

## [202004291952]
### Other
- Republishing due to Git import failing on cljdoc

## [202004291936]
### Removed
- Explicit `org.clojure/clojure` dependency in `deps.edn`

## [202004291641]
### Other
- README version numbers were out of date

## [202004291635]
### Other
- `pom.xml` removed dependency configuration for dependencies that were removed (not tracked in Git)

## [202004291621]
### Other
- `pom.xml` missing dependency configuration for `whoneedszzz/orientdb-client` (not tracked in Git)

## [202004291529]
### Removed
- Migrated connection, transaction, and scripting functionality to `whoneedszzz/orientdb-client` to simplify the
library's responsibility
### Changed
- Updated README documentation to reflect changes
- Moved type hints to `.command` and `.query` method calls to make argument list more readable

## [202004242307]
### Fixed
- Description explicitly states this is a Clojure library

## [202004241531]
### Added
- CHANGELOG 😊
### Changed
- Bumped version in README
### Fixed
- Incorrect doc string in `resultset->maps` to reference Java API rather than the old JDBC API (no impact on functionality)

## [202004241401]
### Added
- Connect to OrientDB instance with or without connection pooling
- Create database
- Drop database
- Execute query
- Execute command
- Execute script
- Begin, commit, and rollback transactions
- Close session
- Close database
- Expand the README to include installation, usage, testing methodology, and issue tracking