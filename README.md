# orientdb-client-sql

An OrientDB library for Clojure using the latest official Java API exclusively using the SQL API, extending 
[whoneedszzz/orientdb-client](https://gitlab.com/whoneedszzz/orientdb-client)

## Why another library?

There are a few libraries that already add support for connecting to OrientDB in Clojure. So you may be
wondering why create another library. The main goal of this library is to remain as simple as possible 
([Simple, Made Easy](https://www.youtube.com/watch?v=oytL881p-nQ)). In that light, this library only
implements functionality for communicating via the Java API using the SQL API exclusively. Other APIs will
be in separate projects (namespaces) if implemented in the future.

There is no extra functionality, such as creating an interface to the Java API object methods via vanilla Clojure 
forms; the library passes OrientDB SQL statements with parameters to the API. If a consumer of the library desires 
this functionality they are welcome to implement that on their end before passing to the client.

A secondary goal is to always use the latest Java API to maintain support of current OrientDB databases.
A consumer of such a library should not need to seek a new library, fork the library, nor wait unreasonably long 
for the library's maintainers to update the driver simply to continue using the library with a current database 
and latest API. Changes to the API libraries will be monitored, but as we are not infallible, should a 
maintainer miss an update, please don't hesitate to politely create an issue that notifies of the update, and 
it will be quickly updated.

## Installation

Deps
```clojure
whoneedszzz/orientdb-client-sql {:mvn/version "202011271332"}
```

Leiningen
```clojure
[whoneedszzz/orientdb-client-sql "202011271332"]
```

## Usage

#### Require
```clojure
(ns oclientsql.test
  (:require [whoneedszzz.orientdb-client.core :as oclient]
            [whoneedszzz.orientdb-client-sql.client :as sql]))
```

### Create connection
```clojure
(def conn (oclient/connect {:url "url" 
                            :root-user "root" 
                            :root-password "password" 
                            :config {} 
                            :db-name "test" 
                            :db-user "admin" 
                            :db-password "admin" 
                            :pool? true}))
```
- See documentation at [whoneedszzz/orientdb-client](https://gitlab.com/whoneedszzz/orientdb-client)

#### Execute query
```clojure
(def query (sql/query {:session (:session conn) 
                       :query "select from Person where firstName = :firstName and lastName = :lastName" 
                       :params {:firstName "John" :lastName "Snow"} 
                       :sort? true}))
```
- Returns a vector of map(s) containing the results
- To not sort output keys set `:sort?` to `false`

#### Execute command
```clojure
(def command (sql/command {:session (:session conn) 
                           :command "create vertex Person set firstName = :firstName, lastName = :lastName" 
                           :params {"firstName" "John" "lastName" "Snow"} 
                           :sort? true}))
```
- Returns a vector of map(s) containing the results
- To not sort output keys set `:sort?` to `false`

### Execute queries and/or commands with a transaction
See documentation at [whoneedszzz/orientdb-client](https://gitlab.com/whoneedszzz/orientdb-client)

### Execute script
See documentation at [whoneedszzz/orientdb-client](https://gitlab.com/whoneedszzz/orientdb-client)

### Create database
See documentation at [whoneedszzz/orientdb-client](https://gitlab.com/whoneedszzz/orientdb-client)

### Drop database
See documentation at [whoneedszzz/orientdb-client](https://gitlab.com/whoneedszzz/orientdb-client)

### Close session
See documentation at [whoneedszzz/orientdb-client](https://gitlab.com/whoneedszzz/orientdb-client)

### Close database
See documentation at [whoneedszzz/orientdb-client](https://gitlab.com/whoneedszzz/orientdb-client)

## Testing

Testing consists of using test data with a real OrientDB database in the REPL during development. Previous REPL 
executions were ran again to ensure regressions did not result after new functionality implementations. The possibility
of subtle timing related issues arising caused automated tests to not be utilized. Desiring to discourage over-confidence
in tests contributed as well. Be confident by verifying each function with your own test data! 😊

## Issues

If you discover any issues or if you believe there are any features missing please create an [Issue](
https://gitlab.com/whoneedszzz/orientdb-client-sql/-/issues/new) and please be reasonably detailed to best address your
input. 😊