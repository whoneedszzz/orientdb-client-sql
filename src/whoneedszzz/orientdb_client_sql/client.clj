(ns whoneedszzz.orientdb-client-sql.client
  (:require [clojure.spec.alpha :as s]
            [whoneedszzz.orientdb-client.core :as oclient])
  (:import [com.orientechnologies.orient.core.db ODatabase
                                                 ODatabaseSession]
           [com.orientechnologies.orient.core.db.document ODatabaseDocumentRemote]
           [java.util Map]))


;; Public functions
(defn command
  "Returns the result of the given session, command, and parameters as a vector of maps optionally sorted and using keywords"
  [{:keys [session command params sort?]}]
  (let [o-params ^Map (oclient/gen-odb-map params)
        result-set (.command ^ODatabaseSession session ^String command o-params)
        results (oclient/resultset->maps result-set sort?)]
    (.close result-set)
    results))

(defn query
  "Returns the result of the given session, query, and parameters as a vector of maps optionally sorted and using keywords"
  [{:keys [session query params sort?]}]
  (let [o-params ^Map (oclient/gen-odb-map params)
        result-set (.query ^ODatabase session ^String query o-params)
        results (oclient/resultset->maps result-set sort?)]
    (.close result-set)
    results))

; Specs
(s/def ::command string?)
(s/def ::query string?)
(s/def ::params map?)
(s/def ::session #(instance? ODatabaseDocumentRemote %))
(s/def ::sort? boolean?)
(s/def ::vec-of-maps (s/coll-of map? :into []))

(s/fdef command
        :args (s/cat :args (s/keys :req-un [::session ::command ::params ::sort?]))
        :ret ::vec-of-maps)
(s/fdef query
        :args (s/cat :args (s/keys :req-un [::session ::query ::params ::sort?]))
        :ret ::vec-of-maps)
